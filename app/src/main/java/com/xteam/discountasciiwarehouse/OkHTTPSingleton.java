package com.xteam.discountasciiwarehouse;

import android.os.Environment;

import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import java.io.File;

/**
 * Created by aditya on 21/11/15.
 */
public class OkHTTPSingleton {

	private static OkHttpClient client;

	private OkHTTPSingleton() {}

	public static OkHttpClient getOkHTTPClient() {
		if(client == null) {
			client = new OkHttpClient();
			createCache();
			client.networkInterceptors().add(new CacheInterceptor());
		}
		return client;
	}

	private static void createCache() {
		int cacheSize = 10*1024*1024;
		Cache cache =  new Cache(getDirectory(), cacheSize);
		client.setCache(cache);
	}

	public static File getDirectory() {
		final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "UCC" + File.separator);
		root.mkdirs();
		final String fname = "httpCache";
		final File sdImageMainDirectory = new File(root, fname);
		return sdImageMainDirectory;
	}

}
