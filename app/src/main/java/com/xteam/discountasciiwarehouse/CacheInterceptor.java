package com.xteam.discountasciiwarehouse;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * Created by aditya on 21/11/15.
 */
public class CacheInterceptor implements Interceptor {
	@Override
	public Response intercept(Chain chain) throws IOException {
		Request request = chain.request();
		request.newBuilder()
				.header("Cache-Control", "only-if-cached")
				.build();
		Response originalResponce = chain.proceed(request);
		return originalResponce.newBuilder()
				.header("Cache-Control", String.format("max-age=%d, only-if-cached, max-stale=%d", 3600, 0))
				.build();
	}
}
