package com.xteam.discountasciiwarehouse;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by aditya on 19/11/15.
 */
public class FaceViewHolder extends RecyclerView.ViewHolder {

	TextView face;

	public FaceViewHolder(View itemView, final FaceAdapter.FaceClickListener clickListener) {
		super(itemView);

		face = (TextView)itemView.findViewById(R.id.faceTextView);
		itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				clickListener.onClick(face, getAdapterPosition());
			}
		});

	}

	public void setText(String message) {
		face.setText(message);
	}

}
