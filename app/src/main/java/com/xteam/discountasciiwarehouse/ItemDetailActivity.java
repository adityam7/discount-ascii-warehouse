package com.xteam.discountasciiwarehouse;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.xteam.discountasciiwarehouse.models.Item;

public class ItemDetailActivity extends AppCompatActivity {

	private TextView faceText;
	private TextView priceText;
	private TextView onlyOneText;
	private LinearLayout buyNowLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_item_detail);

		faceText = (TextView)findViewById(R.id.faceTextView);
		priceText = (TextView)findViewById(R.id.priceTextView);
		onlyOneText = (TextView)findViewById(R.id.only_one);
		buyNowLayout = (LinearLayout)findViewById(R.id.buy_now_layout);

		String data = getIntent().getStringExtra("data");
		Gson gson = new Gson();
		Item item = gson.fromJson(data, Item.class);

		faceText.setText(item.getFace());
		priceText.setText("$"+item.getPrice());

		if(item.getStock() == 1) {
			onlyOneText.setVisibility(View.VISIBLE);
		} else {
			onlyOneText.setVisibility(View.GONE);
		}

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			onBackPressed();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}
