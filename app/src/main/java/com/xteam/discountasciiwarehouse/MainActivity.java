package com.xteam.discountasciiwarehouse;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.xteam.discountasciiwarehouse.asynctasks.LoadItemsTask;
import com.xteam.discountasciiwarehouse.models.Item;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements LoadItemsResponse,
		FaceAdapter.FaceClickListener {

	private ProgressBar progressBar;
	private TextView textView;
	private RecyclerView recyclerView;
	private MenuItem searchMenuItem;
	private LoadItemsTask task;
	private FaceAdapter adapter;
	private List<Item> items;
	private GridLayoutManager manager;
	private boolean isLoading;
	private boolean isLastPage = false;
	private boolean inStock;
	private String searchQuery;
	private EditText searchEditText;

	private RecyclerView.OnScrollListener scrollChangeListener = new RecyclerView.OnScrollListener(){
		@Override
		public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
			super.onScrollStateChanged(recyclerView, newState);
		}

		@Override
		public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
			super.onScrolled(recyclerView, dx, dy);
			int visibleItemCount = manager.getChildCount();
			int totalItemCount = manager.getItemCount();
			int firstVisibleItemPosition = manager.findFirstVisibleItemPosition();

			if (!isLoading && !isLastPage && recyclerView.getVisibility() == View.VISIBLE) {
				if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
						&& firstVisibleItemPosition >= 0) {
					loadMoreItems(items.size());
				}
			}
		}
	};

	private MenuItemCompat.OnActionExpandListener actionExpandListener = new MenuItemCompat.OnActionExpandListener() {
		@Override
		public boolean onMenuItemActionExpand(MenuItem item) {
			return true;
		}

		@Override
		public boolean onMenuItemActionCollapse(MenuItem item) {
			searchQuery = "";
			progressBar.setVisibility(View.VISIBLE);
			textView.setVisibility(View.GONE);
			recyclerView.setVisibility(View.GONE);
			items.removeAll(items);
			loadMoreItems(0);
			return true;
		}
	};

	private TextView.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if (actionId == EditorInfo.IME_ACTION_SEARCH) {
				searchQuery = v.getText().toString();
				if(searchQuery.isEmpty() || searchQuery.equalsIgnoreCase(" ")){
					return true;
				}
				searchQuery = searchQuery.replace(" ", "%20");
				progressBar.setVisibility(View.VISIBLE);
				textView.setVisibility(View.GONE);
				recyclerView.setVisibility(View.GONE);
				items.removeAll(items);
				loadMoreItems(0);
				return true;
			}
			return false;
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		items = new ArrayList<Item>();
		textView = (TextView)findViewById(R.id.textView);
		progressBar = (ProgressBar)findViewById(R.id.progressBar);
		recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
		manager = new GridLayoutManager(this, 2);
		manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
			@Override
			public int getSpanSize(int position) {

				return (position % 3 == 0? 2:1);

			}

		});
		recyclerView.setLayoutManager(manager);
		recyclerView.setHasFixedSize(true);
		recyclerView.addOnScrollListener(scrollChangeListener);
		adapter = new FaceAdapter(items, this);
		recyclerView.setAdapter(adapter);
		getSupportActionBar().setHomeButtonEnabled(false);
		searchQuery = "";
	}

	@Override
	protected void onStart() {
		super.onStart();

		progressBar.setVisibility(View.VISIBLE);
		textView.setVisibility(View.GONE);
		recyclerView.setVisibility(View.GONE);
		if(items.size() == 0) {
			task = new LoadItemsTask(this, 0, inStock, searchQuery);
			isLoading = true;
			task.execute();
		} else {
			progressBar.setVisibility(View.GONE);
			textView.setVisibility(View.GONE);
			recyclerView.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		searchMenuItem = menu.findItem(R.id.search);
		View searchView = searchMenuItem.getActionView();
		searchEditText = (EditText) searchView.findViewById(R.id.editText);
		searchEditText.setOnEditorActionListener(editorActionListener);
		MenuItemCompat.setOnActionExpandListener(searchMenuItem, actionExpandListener);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
			case R.id.in_stock:
				handleInStockedClicked(item);
				return true;
			case android.R.id.home:
				Log.d("HomeClicked", "HomeClicked");
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private void handleInStockedClicked(MenuItem item) {
		inStock = !item.isChecked();
		item.setChecked(!item.isChecked());
		this.items.removeAll(this.items);
		loadMoreItems(0);
	}

	@Override
	public void loadSuccessful( final List<Item> items) {
		if(this.items.size() > 0){
			adapter.removeLoading();
		}
		this.items.addAll(items);
		isLoading = false;
		progressBar.setVisibility(View.GONE);
		textView.setVisibility(View.GONE);
		adapter.notifyDataSetChanged();
		recyclerView.setVisibility(View.VISIBLE);
	}

	@Override
	public void loadComplete() {
		if(items.size() > 1) {
			isLastPage = true;
			isLoading = false;
			adapter.removeLoading();
			adapter.notifyDataSetChanged();
			Snackbar.make(recyclerView, "No more items", Snackbar.LENGTH_SHORT).show();
		} else {
			progressBar.setVisibility(View.GONE);
			textView.setVisibility(View.VISIBLE);
			recyclerView.setVisibility(View.GONE);
			textView.setText("No Items found.");
		}
	}

	@Override
	public void loadFailure(String message) {
		if(isLoading && items.size() == 1) {
			adapter.removeLoading();
		}
		if(items.size()  == 0) {
			isLoading = false;
			progressBar.setVisibility(View.GONE);
			textView.setVisibility(View.VISIBLE);
			recyclerView.setVisibility(View.GONE);
			textView.setText(message);
			textView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					progressBar.setVisibility(View.VISIBLE);
					textView.setVisibility(View.GONE);
					recyclerView.setVisibility(View.GONE);
					loadMoreItems(items.size());
				}
			});
		} else {
			isLoading = false;
			Snackbar.make(recyclerView, message, Snackbar.LENGTH_INDEFINITE)
					.setAction("RETRY", new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							loadMoreItems(items.size());
						}
					}).show();
			adapter.removeLoading();
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(task.getStatus() != AsyncTask.Status.FINISHED)
			task.cancel(true);
	}

	@Override
	public void onClick(View view, int position) {

		Intent intent = new Intent(this, ItemDetailActivity.class);
		Item item = items.get(position);
		Gson gson = new Gson();
		String data = gson.toJson(item, Item.class);
		intent.putExtra("data", data);
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this,
					view, "face");
			startActivity(intent, options.toBundle());
		} else {
			startActivity(intent);
		}

	}

	private void loadMoreItems(int skip) {
		isLoading = true;
		adapter.addLoading();
		adapter.notifyDataSetChanged();
		task = new LoadItemsTask(this, skip, inStock, searchQuery);
		task.execute();
	}

}
