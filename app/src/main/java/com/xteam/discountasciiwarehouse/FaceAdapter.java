package com.xteam.discountasciiwarehouse;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xteam.discountasciiwarehouse.models.Item;

import java.util.List;

/**
 * Created by aditya on 19/11/15.
 */
public class FaceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

	public static final int ITEM = 0;
	public static final int LOADING = 1;

	private boolean mIsLoadingFooterAdded = false;

	private List<Item> faces;
	interface FaceClickListener{
		void onClick(View view, int position);
	}

	FaceClickListener clickListener;

	public FaceAdapter(List<Item> faces, FaceClickListener clickListener){
		this.faces = faces;
		this.clickListener = clickListener;
	}

	private void add(Item face) {
		faces.add(face);
		notifyItemInserted(faces.size()-1);
	}

	public void addLoading(){
		mIsLoadingFooterAdded = true;
		add(new Item());
	}

	public void removeLoading() {
		mIsLoadingFooterAdded = false;

		if(faces.size() > 0) {
			int position = faces.size() - 1;
			Item item = getItem(position);

			if (item != null) {
				faces.remove(position);
				notifyItemRemoved(position);
			}
		}
	}

	Item getItem(int position) {
		return faces.get(position);
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		switch (viewType) {
			case ITEM:
				return createFaceViewHolder(parent);
			case LOADING:
				return createLoadingMoreViewHolder(parent);
			default:
				Log.e("OnCreateViewHolder", "Wrong viewtype: "+viewType);
				return null;
		}

	}

	private FaceViewHolder createFaceViewHolder(ViewGroup parent) {
		View view = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.face_layout, parent, false);
		FaceViewHolder viewHolder = new FaceViewHolder(view, clickListener);
		return viewHolder;
	}

	private LoadingMoreViewHolder createLoadingMoreViewHolder(ViewGroup parent) {
		View view = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.loading_layout, parent, false);
		LoadingMoreViewHolder viewHolder = new LoadingMoreViewHolder(view);
		return viewHolder;
	}

	@Override
	public int getItemViewType(int position) {
		return (position == faces.size()-1 && mIsLoadingFooterAdded)? LOADING : ITEM;
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		switch (getItemViewType(position)) {
			case ITEM:
				bindFaceViewHolder(holder, position);
				break;
			case LOADING:
			default:
				break;

		}
	}

	private void bindFaceViewHolder(RecyclerView.ViewHolder holder, int position) {
		FaceViewHolder faceViewHolder = (FaceViewHolder)holder;
		Item face = faces.get(position);
		faceViewHolder.setText(face.getFace());
	}

	@Override
	public int getItemCount() {
		return (null != faces? faces.size() :0);
	}

	class LoadingMoreViewHolder extends RecyclerView.ViewHolder
	{
		public LoadingMoreViewHolder(View itemView) {
			super(itemView);
		}
	}

}
