package com.xteam.discountasciiwarehouse.models;

import java.util.Arrays;

/**
 * Created by aditya on 19/11/15.
 */
public class Item {

	private String type;
	private String id;
	private int size;
	private float price;
	private String face;
	private int stock;
	private String[] tags;

	public Item() {

	}

	public Item(String type, String id, int size, float price, String face, int stock, String[] tags) {
		this.type = type;
		this.id = id;
		this.size = size;
		this.price = price;
		this.face = face;
		this.stock = stock;
		this.tags = tags;
	}

	public String getType() {
		return type;
	}

	public Item setType(String type) {
		this.type = type;
		return this;
	}

	public String getId() {
		return id;
	}

	public Item setId(String id) {
		this.id = id;
		return this;
	}

	public int getSize() {
		return size;
	}

	public Item setSize(int size) {
		this.size = size;
		return this;
	}

	public float getPrice() {
		return price;
	}

	public Item setPrice(float price) {
		this.price = price;
		return this;
	}

	public String getFace() {
		return face;
	}

	public Item setFace(String face) {
		this.face = face;
		return this;
	}

	public int getStock() {
		return stock;
	}

	public Item setStock(int stock) {
		this.stock = stock;
		return this;
	}

	public String[] getTags() {
		return tags;
	}

	public Item setTags(String[] tags) {
		this.tags = tags;
		return this;
	}

	@Override
	public String toString() {
		return "Item{" +
				"type='" + type + '\'' +
				", id='" + id + '\'' +
				", size=" + size +
				", price=" + price +
				", face='" + face + '\'' +
				", stock=" + stock +
				", tags=" + Arrays.toString(tags) +
				"}\n";
	}
}
