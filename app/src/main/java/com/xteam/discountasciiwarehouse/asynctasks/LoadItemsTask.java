package com.xteam.discountasciiwarehouse.asynctasks;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.CacheControl;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.xteam.discountasciiwarehouse.LoadItemsResponse;
import com.xteam.discountasciiwarehouse.OkHTTPSingleton;
import com.xteam.discountasciiwarehouse.models.Item;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Loads {@link com.xteam.discountasciiwarehouse.models.Item}'s asynchronously either from the
 * server or from memory for the grid.
 */
public class LoadItemsTask extends AsyncTask<Void, Void, List<Item>> {

	private int skip;
	private OkHttpClient client;
	private Call call;
	private Gson gson;
	private LoadItemsResponse loadItemsResponse;
	private Response response;
	private boolean inStock;
	private String searchQuery;

	public LoadItemsTask(LoadItemsResponse loadItemsResponse, int skip, boolean inStock, String searchQuery) throws IllegalArgumentException {
		this.skip = skip;
		this.inStock = inStock;
		this.searchQuery = searchQuery;
		this.loadItemsResponse = loadItemsResponse;
		client = OkHTTPSingleton.getOkHTTPClient();
		gson = new Gson();
	}

	@Override
	protected List<Item> doInBackground(Void... params) {
		String url;
		if(inStock){
			url = "http://74.50.59.155:5000/api/search?limit=10&skip="+skip+"&onlyInStock=1&q="+searchQuery;
		} else {
			url = "http://74.50.59.155:5000/api/search?limit=10&skip="+skip+"&onlyInStock=0&q="+searchQuery;
		}
		Request request = new Request.Builder()
								.url(url)
								.cacheControl(new CacheControl.Builder()
										.maxAge(3600, TimeUnit.SECONDS).build())
								.build();
		try {
			call = client.newCall(request);
			response = call.execute();
			if(response.isSuccessful()){
				String responseString = response.body().string();
				if(responseString.isEmpty()){
					return new ArrayList<Item>();
				}
				responseString = "["+responseString.replace('\n', ',')+"]";
				Type itemListType = new TypeToken<ArrayList<Item>>(){}.getType();
				List<Item> items = gson.fromJson(responseString, itemListType);
				//this is done as a work around because GSON is sometimes adding a null object to
				//the back of the list.
				List<Item> items2 = new ArrayList<Item>();
				for (Item item: items) {
					if(item != null)
						items2.add(item);
				}
				return items2;
			} else {
				Log.e("RESPONCE", response.message());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
		call.cancel();
	}

	@Override
	protected void onPostExecute(List<Item> items) {
		super.onPostExecute(items);

		if(items != null){
			if(items.isEmpty())
				loadItemsResponse.loadComplete();
			else
				loadItemsResponse.loadSuccessful(items);
		} else if(response != null) {
			loadItemsResponse.loadFailure(response.message());
		} else {
			loadItemsResponse.loadFailure("Opps! Something went wrong...\nCheck your internet settings.\nClick here to try again");
		}

	}

}
