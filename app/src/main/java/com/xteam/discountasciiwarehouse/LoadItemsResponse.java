package com.xteam.discountasciiwarehouse;

import com.xteam.discountasciiwarehouse.models.Item;

import java.util.List;

/**
 * Created by aditya on 20/11/15.
 */
public interface LoadItemsResponse {
	void loadSuccessful (List<Item> items);
	void loadComplete();
	void loadFailure (String message);
}
