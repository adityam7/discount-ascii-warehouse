# Discount Ascii Warehouse 

The app should keep loading products from the API until it has enough to fill the screen, and then wait until the user has swiped to the bottom to load more.  The app should cache API requests for 1 hour.

As well as submitting the code, can you also please include:
- any instructions that I'll need to build and run the app (eg. if there's a particular VM I should use)
- notes on technical decisions you made, and the pros / cons of that approach
- recommendations that you would want to see followed up if this were the beginning of a long-term project.  What kinds of things do you look for in the code, technology choices, team practices etc. that can help to make a project successful?

### Building the app
 - Clone the repository
 - Open [Android Studio](http://developer.android.com/sdk/index.html) and click on *Open an existing Android Studio project*
 - Navigate to the project folder and choose it.
 - Let the project open and then run on device or emulator

### Technical Decisions 
Initially I decided to use a database to save the data (Check out the development branch). Then I realised that it's overkill for a small project so I decided to just cache the API requests. For a long term projects where I have multiple types of data to display and have complex searches to perform I would use a database.

### Recommendations
- Response needs to be in JSON format not NDJSON. The reason being there are plenty of tools that help with JSON parsing and even straight away taking the JSON and saving it as objects in your database. Using NDJSON will just slow you down.
- I havent followed any design pattern here but for Android the new hot pattern is MVVM. If this were a long term project I would use MVVM as the pattern while building the app. 
- Where team practices are concerned you **have** to use source control as well as a task tracker.
- If the app requires changes to happen in multiple places when there is some event I would reccomend using rxJava and rxAndroid.
- Again this is something I havent done for this project but TDD is a good way to start building an app as it forces you to write better code.